package com.foodies.dtomapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.foodies.model.UserImage;
import com.foodies.response.UserImageResponse;

@Mapper(componentModel = "spring")
public interface ProfileImageMapper {

    @Mapping(source = "user.id",target = "userId")
    UserImageResponse userImageToResponse(UserImage userImage);

}