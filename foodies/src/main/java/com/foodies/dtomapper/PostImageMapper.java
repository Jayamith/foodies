package com.foodies.dtomapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.foodies.model.PostImage;
import com.foodies.response.PostImageResponse;

@Mapper(componentModel = "spring")
public interface PostImageMapper {

    @Mapping(source = "post.id",target = "postId")
    PostImageResponse imageToResponse(PostImage postImage);

}
