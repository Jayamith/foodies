package com.foodies.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodies.model.PostImage;

import java.util.Optional;

public interface PostImageRepository extends JpaRepository<PostImage, Integer> {
    Optional<PostImage> findPostImageByPost_Id(int postId);
}