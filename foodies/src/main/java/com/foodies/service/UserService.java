package com.foodies.service;

import org.springframework.stereotype.Service;

import com.foodies.dtomapper.UserMapper;
import com.foodies.model.Follow;
import com.foodies.model.User;
import com.foodies.repository.FollowerRepository;
import com.foodies.repository.UserRepository;
import com.foodies.request.UserAddRequest;
import com.foodies.response.UserFollowingResponse;
import com.foodies.response.UserResponse;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
   private final UserMapper userMapper;
   private final UserRepository userRepository;
   private final FollowerRepository followRepository;

    public UserService(UserMapper userMapper, UserRepository userRepository, FollowerRepository followRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.followRepository = followRepository;
    }

    public List<UserResponse> getAll(){

        return userMapper.usersToResponses(userRepository.findAll());
    }
    public UserResponse getResponseById(int id){
        User user = userRepository.findById(id).orElse(null);
        return userMapper.userToResponse(user);
    }

    public UserResponse getByEmail(String email){
        User user = userRepository.findByEmail(email);
        return userMapper.userToResponse(user);
    }

    public List<UserFollowingResponse> getUserFollowing(int userId){
        return userMapper.followsToFollowingResponses(followRepository.findAllByUser_Id(userId));
    }

    public boolean isFollowing(int userId,int followingId){
       Optional<Follow> follow = followRepository.findByUser_IdAndFollowing_Id(userId,followingId);
       return follow.isPresent();
    }

    public User getById(int id){
        return userRepository.findById(id).get();
    }
    public void add(UserAddRequest userAddRequest){
        User user = userMapper.requestToUser(userAddRequest);
        userRepository.save(user);
    }

    public void delete(int id){
        userRepository.deleteById(id);
    }
}

