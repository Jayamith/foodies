package com.foodies.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.foodies.dtomapper.ProfileImageMapper;
import com.foodies.model.UserImage;
import com.foodies.repository.UserImageRepository;
import com.foodies.response.UserImageResponse;
import com.foodies.utils.ImageUtil;

import java.io.IOException;
import java.util.Optional;

@Service
public class UserImageService {

    private final UserImageRepository userImageRepository;
    private final UserService userService;
    private final ProfileImageMapper profileImageMapper;

    public UserImageService(UserImageRepository userImageRepository, UserService userService, ProfileImageMapper profileImageMapper) {
        this.userImageRepository = userImageRepository;
        this.userService = userService;
        this.profileImageMapper = profileImageMapper;
    }

    public UserImageResponse upload(MultipartFile file,int userId) throws IOException {
        UserImage userImage = new UserImage();
        userImage.setData(ImageUtil.compressImage(file.getBytes()));
        userImage.setName(file.getOriginalFilename());
        userImage.setType(file.getContentType());
        userImage.setUser(userService.getById(userId));
        userImageRepository.save(userImage);
        return profileImageMapper.userImageToResponse(userImage);
    }

    public byte[] download(int id){
        Optional<UserImage> userImage = userImageRepository.findByUser_Id(id);
        return ImageUtil.decompressImage(userImage.get().getData());
    }
}
