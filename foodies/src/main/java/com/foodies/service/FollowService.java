package com.foodies.service;


import org.springframework.stereotype.Service;

import com.foodies.dtomapper.FollowerMapper;
import com.foodies.model.Follow;
import com.foodies.repository.FollowerRepository;
import com.foodies.request.FollowRequest;


@Service
public class FollowService {
    private final FollowerRepository followRepository;
    private final FollowerMapper followerMapper;
    private final UserService userService;

    public FollowService(FollowerRepository followRepository, FollowerMapper followerMapper, UserService userService) {
        this.followRepository = followRepository;
        this.followerMapper = followerMapper;
        this.userService = userService;
    }

    public void add(FollowRequest followAddRequest){
        if (userService.isFollowing(followAddRequest.getUserId(), followAddRequest.getFollowingId())){
            return;
        }
        followRepository.save(followerMapper.addRequestToFollow(followAddRequest));
    }

    public  void delete(FollowRequest followRequest){
      Follow follow
                = followRepository.findByUser_IdAndFollowing_Id(followRequest.getUserId(), followRequest.getFollowingId()).orElse(null);
        followRepository.delete(follow);
    }


}
